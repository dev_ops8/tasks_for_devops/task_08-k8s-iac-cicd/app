## Application

App consist of React as a front, flask as a back. App connected to mysql database.
If run it at first time, you need do migrations to db. Fisrt of all, enter to container and consider that you can connect to db. Then run
"<u>flask db upgrade</u>". 
Take convince that migrations was successfull.

## Deploy app on localhost

If you want deploy application on localhost, you need remember that application needs some environment variables. If there are not use, you get an error.

Must have variables to successfully run on localhost:

- SECRET_KEY: 45946859grfgltht,*&#RKmdkfvfv^($)kdkv
    any secret key you want use with any symbols

- DATABASE_URL : mysql://user:password@host:port/db
    for our configuration we use mysql db, but you can use whatever you want. So if you want use another db, please don't forget to 
    change python code and environment variables;

- FLASK_APP: app

- FLASK_ENV: production

- SQLALCHEMY_ECHO: "True"