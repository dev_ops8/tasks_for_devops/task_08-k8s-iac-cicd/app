import os
import socket
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request, session, redirect, jsonify, url_for
from flask_cors import CORS
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect, generate_csrf
from flask_login import LoginManager
from .Socket import socketio
from datetime import datetime, date
from flask_restful import Resource, Api

from .models import chat, message, pet, user_chat, user
from .api.user_routes import user_routes
from .api.auth_routes import auth_routes
from .api.users_routes import users_routes
from .api.message_routes import message_routes
from .api.chat_routes import chat_routes

from .seeds import seed_commands

from .config import Config

app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
api = Api(app)
# Setup login manager
login = LoginManager(app)
login.login_view = 'auth.unauthorized'
db = SQLAlchemy(app)
# migrate = Migrate(app, db)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))


# Tell flask about our seed commands
app.cli.add_command(seed_commands)

app.config.from_object(Config)
app.register_blueprint(user_routes, url_prefix='/api/users')
app.register_blueprint(auth_routes, url_prefix='/api/auth')
app.register_blueprint(users_routes, url_prefix='/api/all_users')
app.register_blueprint(message_routes, url_prefix='/api/messages')
app.register_blueprint(chat_routes, url_prefix='/api/chats')
# db.create_all()
db.init_app(app)
Migrate(app, db)
socketio.init_app(app)

# Testing zone time-api
class TimeV1(Resource):
    def get(self):
        m = {'datetime':format(datetime.isoformat(datetime.now())),
             'version':1,
             'hostname':socket.gethostname()}
        return jsonify(m)

api.add_resource(TimeV1, '/api/v1/')

# Class Pets
class Pet(db.Model):
    __tablename__ = 'pets'

    id = db.Column(db.Integer, primary_key = True, nullable = False)
    owner_id = db.Column(db.Integer, nullable=False, default=1)
    name = db.Column(db.String(50), nullable = False)
    image = db.Column(db.String(360))
    bio = db.Column(db.Text, nullable = False, default="Please Fill Me Out")
    age = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, nullable = False, default=date.today())
    updated_at = db.Column(db.DateTime, nullable = False, default=date.today())

    def to_dict(self):
        return {
        "id": self.id,
        "owner_id": self.owner_id,
        "name": self.name,
        "image": self.image,
        "bio": self.bio,
        "age": self.age,
        }


# Route to list Pets
@app.route('/pets', methods=['GET'])
def pets():
    return render_template('pets.html', pets=Pet.query.all())

# Route to Add Pets
@app.route('/add_pets', methods=['POST'])
def add_pet():
    name = request.form['name']
    bio = request.form['bio']
    age = request.form['age']
    id_form = request.form['id_form']

    db.session.add(Pet(id=id_form, name=name, bio=bio, age=age))
    db.session.commit()

    return redirect(url_for('pets'))

# Application Security
CORS(app)

# Since we are deploying with Docker and Flask,
# we won't be using a buildpack when we deploy to Heroku.
# Therefore, we need to make sure that in production any
# request made over http is redirected to https.
# Well.........


#@app.before_request
#def https_redirect():
#    if os.environ.get('FLASK_ENV') == 'production':
#        if request.headers.get('X-Forwarded-Proto') == 'http':
#            url = request.url.replace('http://', 'https://', 1)
#            code = 301
#            return redirect(url, code=code)


@app.after_request
def inject_csrf_token(response):
    response.set_cookie('csrf_token',
                        generate_csrf(),
                        secure=True if os.environ.get('FLASK_ENV') == 'production' else False,
                        samesite='Strict' if os.environ.get('FLASK_ENV') == 'production' else None,
                        httponly=True)
    return response


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def react_root(path):
    print("path", path)
    if path == 'favicon.ico':
        return app.send_static_file('favicon.ico')
    return app.send_static_file('index.html')


if __name__ == '__main__':
    socketio.run(app)
