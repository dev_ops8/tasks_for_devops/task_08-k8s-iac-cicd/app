# task_08-k8s-cicd-iac-web-app
This task simulates the real project of dockerized app

## Description of the task
We have small team includes devops group and developer's group. We must create CI/CD pipeline that can do ordinary things such as testing the developer's code, building docker images, push it to our registry and etc. But we must create the CD part, that will deploy our dockerized app to real "dev" environment on cloud provider, trigger by any commit in "<u>dev</u>" branch in <u>app repository</u> via developer's group.
So, our CI/CD pipeline can linting dev code, build docker images, push to registry and deploy it to "dev" environment on cloud provider via any commit in "dev" branch in <u>app repository</u> via developer's group. Our developer's are professional, so they always add tag's when commit to "dev" branch. When our CI/CD pipeline will deploy to cloud provider, we must reuse this tag, as a tag for our deployed apps on cloud provider environment, it gives us possibility to follow for our versioning of app. 
Also, if we must update/change K8s ifrastructure via devops team, we must be able to find the "release" version of infrastructure.
If any wrong will happen and our K8s infra down, we must rollback to previous version. 
Also we must save log's from our app to any persistent volume. It help us to analyze the behaviour of app.
Files for app take from task_04

## Check-points:
- Separate on different repository via purpose of code: app - for app code, IaC - for infra code, K8s - for K8s code
- Create CI/CD pipeline that will linting app code, build images and push to registry by any commint in branch "<u>dev</u>" in app repository
- Create CI/CD pipeline that can deploy/update version of app trigger by commits in <u>app repository</u>
- Create CI/CD pipeline that can deploy/update version of K8s infra trigger by commits in <u>K8s repository</u>  
- Create CI/CD pipeline that can deploy infrastructure in cloud provider trigger by commits in <u>IaC repository</u> 
- Create simple logging system to save log's of pod's(fluentd or etc)

## Intro and general steps of provision K8s cluster
This repository define for storage of code for our application. There are some rules:
- Developer's can create any branch or etc
- When they want to push the define version of app to <u>dev</u> environment, they must add tag with "<u>-dev</u>" on the end
- If they want to update <u>prod</u> version, they add tag with "<u>-prod</u>" on the end
- Then CI/CD pipeline run and try to update version of app.

Basic steps:
- Test app code 
- Build image and push to registry
- Deploy image to dev or prod environment depends on the tag commit

## Attention
The version of deploying app to dev environment must be in format: [a-z].[0-9].[0-9].[0-9]-dev
The version of deploying app to prod environment must be in format: [a-z].[0-9].[0-9].[0-9]-prod
